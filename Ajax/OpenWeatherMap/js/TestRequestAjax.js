/*
    Test requete Ajax
    Site Web
    Julien LESSART
    Version 1.0
    03/05/20
*/

//Déclarations des variables
let urlRequete5j = "", // Url pour les 5 prochains jours
    urlRequeteta = "", // Url pour le temps actuel
    url, // Ville donnée par l'utilisateur
    keyOpenWeatherMap = "", // Ma clé openweathermap voir Key.txt
    longitude = "", // Longitude de la ville
    latitude = "", // Latitude de la ville
    nom, // Nom de la ville
    pays; //Pays de la ville



$(document).ready(function() { // Pour charger le Jquery au chargement de la page

    $("#btnVille").on("click", function() { // Quand on clique sur btnChercher
        CheckList();
        Reset();
        url = $("#EdtVille").val();
        keyOpenWeatherMap = $("#EdtOwm").val();
        urlRequete5j = "http://api.openweathermap.org/data/2.5/forecast?q=" + url.toString() + ",fr&lang=fr&APPID=" + keyOpenWeatherMap; // Url 5 jours
        urlRequeteta = "http://api.openweathermap.org/data/2.5/weather?q=" + url.toString() + ",fr&lang=fr&APPID=" + keyOpenWeatherMap; // Url temps actuel

        // Requete ajax 5 jours
        $.ajax({
            type: "GET",
            url: urlRequete5j,
            success: onGetCommuneSuccess5j,
            error: onGetCommuneError5j
        });

        // Requete ajax temps actuelle
        $.ajax({
            type: "GET",
            url: urlRequeteta,
            success: onGetCommuneSuccessta,
            error: onGetCommuneErrorta
        });
    });

    $("form").on("click", function() { // Quand on clique sur la checkbox Temps actuel
        CheckList();
    });
});

// Si succes 5 jours
function onGetCommuneSuccessta(reponse, status) {
    $("#divResult").append("<p id=" + "Titre" + ">" + reponse.name + "," + reponse.sys.country + "</p>");
    $("#divResult").append(" <p id=" + "Titre2" + "><img/>" + (reponse.main.temp - 273).toFixed(1) + "°C</p>");
    $("#divResult p:last-child img").attr("src", "http://openweathermap.org/img/w/" + reponse.weather[0].icon + ".png");
    $("#divResult").append("<p>" + reponse.weather[0].main + "</p>");

    $("#divResult").append("<table> </table>");
    $("#divResult table").append("<tr> <td> Wind </td> <td>" + reponse.wind.speed + "</td></tr>");
    $("#divResult table").append("<tr> <td> Cloudiness </td> <td>" + reponse.weather[0].description + "</td></tr>");
    $("#divResult table").append("<tr> <td> Pressure </td> <td>" + reponse.main.pressure + " hpa</td></tr>");
    $("#divResult table").append("<tr> <td> Humidity </td> <td>" + reponse.main.humidity + " %</td></tr>");
    $("#divResult table").append("<tr> <td> Sunrise </td> <td>" + new Date(reponse.sys.sunrise * 1000) + "</td></tr>");
    $("#divResult table").append("<tr> <td> Sunset </td> <td>" + new Date(reponse.sys.sunset * 1000) + "</td></tr>");
    $("#divResult table").append("<tr> <td> Geo coords </td> <td> [" + reponse.coord.lon + ", " + reponse.coord.lat + "]" + "</td></tr>");

    longitude = reponse.coord.lon;
    latitude = reponse.coord.lat;
    nom = reponse.name;
    pays = reponse.sys.country;
    initMap();
}

// Si échec 5 jours
function onGetCommuneErrorta(status) {
    alert(JSON.stringify(status));
}

// Si succes 5 jours
function onGetCommuneSuccess5j(reponse, status) {
    GraphHighcharts(reponse);
}

// Si échec 5 jours
function onGetCommuneError5j(status) {
    alert(JSON.stringify(status));
}

function GraphHighcharts(reponse) {
    Highcharts.chart('container', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Tempèrature des 5 prochains jours'
        },
        subtitle: {
            text: 'Source: openweathermap'
        },
        xAxis: {
            categories: ['J1', 'J2', 'J3', 'J4', 'J5']
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: $("#EdtVille").val(),
            data: [parseInt(reponse.list[0].main.temp) - 273, parseInt(reponse.list[1].main.temp) - 273, parseInt(reponse.list[2].main.temp) - 273, parseInt(reponse.list[3].main.temp) - 273, parseInt(reponse.list[4].main.temp) - 273]
        }, ]
    });
}


function initMap() { // Créer l'objet "map" et l'insèrer dans l'élément HTML qui a l'ID "map" 
    var map = L.map('macarte').setView([latitude, longitude], 13);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    L.marker([latitude, longitude]).addTo(map)
        .bindPopup(nom + " " + pays)
        .openPopup();
}


function Reset() {
    // Permet de reset l'html ajouter avec le script
    // Aucun prm
    // Aucune valeur de retour

    $("#divResult p").remove();
    $("table").remove();
    $("#container div").remove();
    $("#macarte").remove();
    $("figure").after("<div id=" + "macarte" + "></div>"); // Pour regler le probléme de map grise
}

function CheckList() {
    // Permet de savoir ce que l'utilisateur veut afficher
    // Aucun prm
    // Aucune valeur de retour

    if ($("#CbTa").is(":checked")) {
        $("#divResult").show();
    } else {
        $("#divResult").hide();
    }

    if ($("#Cb5j").is(":checked")) {
        $("#container").show();
    } else {
        $("#container").hide();
    }

    if ($("#CbGm").is(":checked")) {
        $("#macarte").show();
    } else {
        $("#macarte").hide();
    }
}