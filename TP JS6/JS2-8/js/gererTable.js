/*
    Script javascript 
    Permet d'ajouter, supprimer des lignes et d'avoir la somme des valeurs a l'intérieure du tableau
    29/04/20
    Julien Lessart 
    Version 2.0 utilisant les fonctionalités de JS6
*/

let tBody = document.querySelector("#Tableau1 tbody");

document.getElementById("btnAjouter").addEventListener('click', function ajouterligne() {
    // Fonction permettant d'ajouter une ligne
    // Aucun paramètre
    // Aucune valeur de retour

    // Déclarations des variables
    let tr = document.createElement("tr"), // Déclaration de la nouvelle balise tr
        td = document.createElement("td"), // Déclaration de la nouvelle balise td
        txtTd = document.createTextNode(tBody.childElementCount + 1); // Incrémentation de la valeur de td

    // ajout sur la page web    
    td.appendChild(txtTd); // Pour que le noeud txtTd soit ajouter au noeud parent
    tr.appendChild(td); // Pour que le noeud td soit ajouter au noeud parent
    tBody.appendChild(tr); // Pour que le noeud tr soit ajouter au noeud parent

    calculerTotal(); // On appel la fonction calculerTotal qui permet de calculer le total des valeurs du tableau
});

document.getElementById("btnSupprimer").addEventListener('click', function ajouterligne() {
    // Fonction permettant de supprimer une ligne
    // Aucun paramètre
    // Aucune valeur de retour

    // Supprimer des tr/td
    tBody.removeChild(tBody.lastElementChild); // On supprime le dernier noeud
    calculerTotal(); // On appel la fonction calculerTotal qui permet de calculer le total des valeurs du tableau
});

function calculerTotal() {
    // Fonction permettant de calculer la somme total
    // Aucun paramètre
    // Aucune valeur de retour

    // Déclarations des variables
    let somme = 0, // somme des td afficher dans le tfoot
        test = document.querySelectorAll("#Tableau1 tbody tr td");

    // Boucle pour faire la somme des td un par un
    for (let i = 0; i < parseInt(tBody.childElementCount); i++) {
        somme += parseInt(test[i].lastChild.nodeValue);
    }

    // Affichage final
    document.querySelector("#Tableau1 tfoot tr td").innerHTML = somme;
}