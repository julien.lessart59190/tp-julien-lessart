/*
    Script JavaScript "afficherDiv.js"
    Script permettant d'ouvrir, fermer une div en appuyant sur un bouton
    29/04/20
    Julien LESSART
    Version 2.0 utilisant JS 6
*/

// Déclarations des variables

let div = document.getElementById("divText"),
    bouton = document.getElementById("btn1");

bouton.onclick = function OuvrirDiv() {
    // Fonction permettant d'ouvrir la div
    // Aucun paramètre
    // Aucune valeur de retour

    // Si la div est non afficher 
    if (div.style.display === "none") {
        div.style.display = "block";
        bouton.value = "Ouvrir"
    } else {
        div.style.display = "none";
        bouton.value = "Fermer"
    }
}