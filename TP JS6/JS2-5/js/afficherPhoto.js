/*
    Script JavaScript "afficherPhoto.js"
    Script permettant d'afficher une photo après clique sur un lien
    29/04/20
    Julien LESSART
    Version 2.0 utilisant JS 6
*/

// Déclarations des variables
let photo = document.getElementById("photo");

photo.onclick = function afficherPhoto(event) {
    // Fonction permettant d'afficher une photo quand on clique sur un lien
    // Aucun paramètre
    // Aucune valeur de retour

    event.preventDefault(); // Pour désactiver le comportement standart du lien (Pour garder l'image)

    //déclarations des variables 
    let img = document.createElement("img");
    img.setAttribute("src", "photo.jpg");

    // Pour remplacer le lien par l'image
    photo.replaceWith(img);

}