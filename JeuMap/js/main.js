/*
    Script JavaScript
    Permet de géolocaliser et de créer une map
    Pour savoir jusqu'à quelle distance vous pouvez allez
    Julien LESSART
    11/05/20
    Version 1.0
*/

$(document).ready(function() { // Pour charger le Jquery au chargement de la page

    // Déclarations des variables
    let carte = "", // Pour pouvoir modifier la carte 
        marker = "", // Pour gerer le marqueur de l'utilisateur
        markerVille = "", // Pour gerer le marqueur de la ville
        cpt = 1, // Pour le nombre de tour de jeu (10) commence à 1 pour le compteur 1 Lille, 2 Paris ect
        idVille = -1, // Pour avoir l'id(index) de la ville séléctionner
        temps = 10, // Pour le temps en secondes 
        points = 0, // Le nombre de points du joueur
        interval = 0, // L'interval de chaque tick 
        arrayVille = []; // Le tableau contenant les villes
    // [ [[id => 0],[latitude => 0], [longitude => 0], [nom => "Paris"], [selec => 0]],
    //   [[id => 1],[latitude => 0], [longitude => 0], [nom => "Marseille"], [selec => 0]],
    //   ...
    //   [[id => 49],[latitude => 0], [longitude => 0], [nom => "Dunkerque"], [selec => 0]]];
    // Après la selection des 10 villes
    // [ [[id => 0],[latitude => 48.8589], [longitude => 2.3469], [nom => "Paris"], [selec => 1]],
    //   [[id => 1],[latitude => 43.2804], [longitude => 5.3806], [nom => "Marseille"], [selec => 0]],
    //   ...
    //   [[id => 49],[latitude => 0], [longitude => 0], [nom => "Dunkerque"], [selec => 1]]];



    initVille(); // On ajoute les villes au tableau des villes
    initMap(); // Puis on appel initMap pour créer la carte



    // Gestion de la séléction des villes
    function initVille() {
        // Permet de créer les tableaux de valeurs des villes
        // ArrayVille est fait d'une id(même valeur que son index qui permet de se rapeler de la ville sélectionner)
        // latitude et longitude sont les coordonées de la ville trouvée avec comarquage
        // selec est un booléen pour savoir si la ville à déjà était sélectionner (pour ne pas avoir de doublon)
        // nom est le nom de la ville

        arrayVille.push({ id: 0, latitude: 0, longitude: 0, selec: 0, nom: "Paris" });
        arrayVille.push({ id: 1, latitude: 0, longitude: 0, selec: 0, nom: "Marseille" });
        arrayVille.push({ id: 2, latitude: 0, longitude: 0, selec: 0, nom: "Lyon" });
        arrayVille.push({ id: 3, latitude: 0, longitude: 0, selec: 0, nom: "Toulouse " });
        arrayVille.push({ id: 4, latitude: 0, longitude: 0, selec: 0, nom: "Nice " });
        arrayVille.push({ id: 5, latitude: 0, longitude: 0, selec: 0, nom: "Nantes" });
        arrayVille.push({ id: 6, latitude: 0, longitude: 0, selec: 0, nom: "Montpellier " });
        arrayVille.push({ id: 7, latitude: 0, longitude: 0, selec: 0, nom: "Strasbourg " });
        arrayVille.push({ id: 8, latitude: 0, longitude: 0, selec: 0, nom: "Bordeaux " });
        arrayVille.push({ id: 9, latitude: 0, longitude: 0, selec: 0, nom: "Lille " });
        arrayVille.push({ id: 10, latitude: 0, longitude: 0, selec: 0, nom: "Rennes" });
        arrayVille.push({ id: 11, latitude: 0, longitude: 0, selec: 0, nom: "Reims" });
        arrayVille.push({ id: 12, latitude: 0, longitude: 0, selec: 0, nom: "Saint-Étienne " });
        arrayVille.push({ id: 13, latitude: 0, longitude: 0, selec: 0, nom: "Toulon" });
        arrayVille.push({ id: 14, latitude: 0, longitude: 0, selec: 0, nom: "Le Havre " });
        arrayVille.push({ id: 15, latitude: 0, longitude: 0, selec: 0, nom: "Grenoble" });
        arrayVille.push({ id: 16, latitude: 0, longitude: 0, selec: 0, nom: "Dijon" });
        arrayVille.push({ id: 17, latitude: 0, longitude: 0, selec: 0, nom: "Angers" });
        arrayVille.push({ id: 18, latitude: 0, longitude: 0, selec: 0, nom: "Nîmes" });
        arrayVille.push({ id: 19, latitude: 0, longitude: 0, selec: 0, nom: "Saint-Denis " });
        arrayVille.push({ id: 20, latitude: 0, longitude: 0, selec: 0, nom: "Villeurbanne " });
        arrayVille.push({ id: 21, latitude: 0, longitude: 0, selec: 0, nom: "Clermont-Ferrand " });
        arrayVille.push({ id: 22, latitude: 0, longitude: 0, selec: 0, nom: "Le Mans" });
        arrayVille.push({ id: 23, latitude: 0, longitude: 0, selec: 0, nom: "Aix-en-Provence " });
        arrayVille.push({ id: 24, latitude: 0, longitude: 0, selec: 0, nom: "Brest " });
        arrayVille.push({ id: 25, latitude: 0, longitude: 0, selec: 0, nom: "Tours" });
        arrayVille.push({ id: 26, latitude: 0, longitude: 0, selec: 0, nom: "Amiens" });
        arrayVille.push({ id: 27, latitude: 0, longitude: 0, selec: 0, nom: "Limoges" });
        arrayVille.push({ id: 28, latitude: 0, longitude: 0, selec: 0, nom: "Annecy" });
        arrayVille.push({ id: 29, latitude: 0, longitude: 0, selec: 0, nom: "Perpignan" });
        arrayVille.push({ id: 30, latitude: 0, longitude: 0, selec: 0, nom: "Boulogne-Billancourt" });
        arrayVille.push({ id: 31, latitude: 0, longitude: 0, selec: 0, nom: "Orléans" });
        arrayVille.push({ id: 32, latitude: 0, longitude: 0, selec: 0, nom: "Metz" });
        arrayVille.push({ id: 33, latitude: 0, longitude: 0, selec: 0, nom: "Besançon" });
        arrayVille.push({ id: 34, latitude: 0, longitude: 0, selec: 0, nom: "Saint-Denis" });
        arrayVille.push({ id: 35, latitude: 0, longitude: 0, selec: 0, nom: "Argenteuil" });
        arrayVille.push({ id: 36, latitude: 0, longitude: 0, selec: 0, nom: "Rouen" });
        arrayVille.push({ id: 37, latitude: 0, longitude: 0, selec: 0, nom: "Montreuil" });
        arrayVille.push({ id: 38, latitude: 0, longitude: 0, selec: 0, nom: "Mulhouse" });
        arrayVille.push({ id: 39, latitude: 0, longitude: 0, selec: 0, nom: "Caen" });
        arrayVille.push({ id: 40, latitude: 0, longitude: 0, selec: 0, nom: "Saint-Paul" });
        arrayVille.push({ id: 41, latitude: 0, longitude: 0, selec: 0, nom: "Nancy" });
        arrayVille.push({ id: 42, latitude: 0, longitude: 0, selec: 0, nom: "Tourcoing" });
        arrayVille.push({ id: 43, latitude: 0, longitude: 0, selec: 0, nom: "Roubaix" });
        arrayVille.push({ id: 44, latitude: 0, longitude: 0, selec: 0, nom: "Nanterre" });
        arrayVille.push({ id: 45, latitude: 0, longitude: 0, selec: 0, nom: "Poitiers" });
        arrayVille.push({ id: 46, latitude: 0, longitude: 0, selec: 0, nom: "Créteil" });
        arrayVille.push({ id: 47, latitude: 0, longitude: 0, selec: 0, nom: "Avignon" });
        arrayVille.push({ id: 48, latitude: 0, longitude: 0, selec: 0, nom: "Vitry-sur-Seine" });
        arrayVille.push({ id: 49, latitude: 0, longitude: 0, selec: 0, nom: "Dunkerque" });

        // Ensuite on recherche les latitudes et longitude de chaque ville grace à leurs nom
        for (let i = 0; i < arrayVille.length; i++) {
            // Requete ajax
            $.ajax({
                type: "GET",
                url: `http://ou.comarquage.fr/api/v1/autocomplete-territory?kind=CommuneOfFrance&term= ${arrayVille[i].nom}`,
                dataType: "jsonp",
                jsonp: "jsonp",
                success: onGetCommuneSuccess,
                error: onGetCommuneError
            });

            // Si succes 
            function onGetCommuneSuccess(reponse) {
                arrayVille[i].latitude = reponse.data.items[0].latitude; // on ajoute a la fin de la divResult la réponse avec l'item i pour chaque main_postal_distribution
                arrayVille[i].longitude = reponse.data.items[0].longitude;
            }

            // Si échec
            function onGetCommuneError() {
                alert("Problème dans la localisation de la ville");
            }
        }

    }

    function initMap() {
        //Créer un objet "map" et l'insèrer dans l'élément HTML qui a l'ID "macarte" 

        // Affectations des variables
        carte = L.map('macarte', { // La carte
            maxBounds: L.latLngBounds(L.latLng(55.511125799286326, -14.025788027470686), L.latLng(36.42840435364896, 16.968899545991206)), // Le déplacement max
            minZoom: 5, // Le zoom min (dézoom)
            maxZoom: 22 // Le zoom max (zoomer)
        }).setView([46.9045897, 2.3829137], 5);
        L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(carte); // On l'ajoute a la carte
        marker = L.marker([0, 0], {
            icon: L.icon({
                iconUrl: 'Leaflet/images/marker-icon.png', // Design du marqueur
                iconSize: [25, 41], // Taille du marqueur
                iconAnchor: [12, 41], // Point du marqueur
                popupAnchor: [-3, -76] // Distance entre le marqueur et le popup
            })
        }).addTo(carte); // Pour le marqueur
        $("#divRes").prepend(`<p>Situez 10 villes de France sur la carte </p> <p>Vous avez 10 secondes par ville. </p> <p>Votre score final dépend de votre précision </p> <p>et de votre rapidité </p>`);
        $("#btnLancer").on("click", function() {
            $("#divRes").hide();
            $("#divPoints").html(0 + " pts");
            VilleJeu();
        });
    }

    function getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

    function VilleJeu() {
        if (cpt <= 10) {
            let random = getRandomInt(arrayVille.length);
            while (arrayVille[random].selec !== 0) {
                random = getRandomInt(arrayVille.length);
            }
            if (arrayVille[random].selec === 0) {
                arrayVille[random].selec = 1;
                idVille = arrayVille[random].id;
                $("#divVille").html(`${cpt} : ${arrayVille[random].nom} ?`);
                cpt++;
                start();
            }
        } else {
            alert("Bravo vous avez fait : " + points + " points");
        }
    }

    function start() {
        interval = setInterval(tick, 1000);
        temps = 10;

    }

    function tick() {
        temps--;
        carte.on('click', placerMarqueur);
        if (temps == 0) finish();
        else {
            $("#divTemps").html(temps);
        }
    }

    function finish() {
        clearInterval(interval);
        $("#divRes ").show();
        $("#divRes").html(`Temps écoulé 0 points`);
        $("#divRes").css("width", 100);
        $("#divRes").css("height", 45);
        $("#divRes").css("margin-left", 150);
        setTimeout(function() {
            $("#divRes").hide();
            VilleJeu();
        }, 3000);
    }



    function placerMarqueur(e) {
        marker.setLatLng(e.latlng);
        clearInterval(interval);
        markerVille = L.marker([arrayVille[idVille].latitude, arrayVille[idVille].longitude], {
            icon: L.icon({
                iconUrl: 'Leaflet/images/marker-icon-2x.png', // Design du marqueur
                iconSize: [25, 41], // Taille du marqueur
                iconAnchor: [12, 41], // Point du marqueur
                popupAnchor: [-3, -76] // Distance entre le marqueur et le popup
            })
        }).addTo(carte); // Pour le marqueur

        let resultatKm = Math.round(marker.getLatLng().distanceTo(markerVille.getLatLng()) / 1000.0);
        let pointsTour = 0;
        if (resultatKm < 200) {
            pointsTour = Math.floor(100 / resultatKm / 0.01 - (temps / 10))
            points += pointsTour;
        }
        $("#divPoints").html(points + " pts");
        $("#divRes").show();
        $("#divRes").html(`<p> ${resultatKm} kms - ${10 - temps}sec </p> <p> ${pointsTour}pts </p>`);
        $("#divRes").css("width", 100);
        $("#divRes").css("height", 75);
        $("#divRes").css("margin-left", 200);
        setTimeout(function() {
            $("#divRes").hide();
            marker.setLatLng(L.latLng(0, 0));
            markerVille.remove();
            VilleJeu();
        }, 3000);
    }

});