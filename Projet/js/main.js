/*
    Script JavaScript
    Permet de géolocaliser et de créer une map
    Pour savoir jusqu'à quelle distance vous pouvez allez
    Julien LESSART
    11/05/20
    Version 1.0
*/

$(document).ready(function() { // Pour charger le Jquery au chargement de la page

    // Déclarations des variables
    let latitude = 0, // Latitude en gon
        longitude = 0, // Longitude en gon
        reponseRecherche = "", // La réponse Ajax de la recherche manuelle
        reponseWScontoursDpt = "", // La réponse Ajax des polygones pour les contours au format web service
        reponseWSAdresses = "", // La réponse Ajax du géocodage inverser des adresses au format web service
        carte = "", // Pour pouvoir modifier la carte 
        cercle = "", // Pour garder en mémoire le cercle de la map
        marker = "", // Pour garder en mémoire le marqueur de la map
        arrayLeafletPolygon = [], // Tableau qui garde chaque polygone au format L.polygon 
        polygone = [], // Prend les valeurs d'un polygone à chaque fois pour les intégrer au tableau des polygones au format L.latlng polygone[i][0] pour lat polygone[i][1]
        dataPolygon = "", // Pour extraires les données du / des polygones
        cpt = 0, // Permet de savoir si on à déjà init la map
        veriflocalisation = 0; // Si on veut localiser ou rechercher

    if (navigator.geolocation) { // Si on peut effectuer une geolocalisation
        navigator.geolocation.getCurrentPosition(maPosition, erreurPosition); // on exécute soit maPosition(succes) erreurPosition(échec)
    } else { // Sinon
        alert("Ce navigateur ne supporte pas la géolocalisation"); // Afficher erreur
    }

    function maPosition(position) {
        // Permet de savoir la géolocalisation
        // Un paramètre la position

        latitude = position.coords.latitude; // On affecte la valeur trouver en latitude 
        longitude = position.coords.longitude; // On affecte la valeur trouver en longitude
        initContour(); // Appel de la fonction initContour
    }

    function erreurPosition(error) {
        // pour gerer les erreurs 
        var info = "Erreur lors de la géolocalisation : ";
        switch (error.code) { // Switch du code d'erreur
            case error.TIMEOUT:
                info += "Timeout !"; // Si le temps est écoulé
                break;
            case error.PERMISSION_DENIED:
                info += "Vous n’avez pas donné la permission"; // Si on a pas la permision 
                break;
            case error.POSITION_UNAVAILABLE:
                info += "La position n’a pu être déterminée"; // Si la position n'a pas était déterminée
                break;
            case error.UNKNOWN_ERROR:
                info += "Erreur inconnue"; // Si on a une erreur inconnu
                break;
        }
    }

    $("#EdtPrincipal").keydown(function(event) {
        // Si on clique sur une touche quand on est en train de taper du texte dans l'edt
        window.scrollTo(0, 0); // On remonte la page(pour téléphone)
        if (event.keyCode == 13) { // Si on appui sur entre (13 code ASCII)
            $("#BtnRechercher").click(); // On simule le click sur BtnRechercher
        }
    });

    $("#BtnRechercher").on("click", function() {
        // Quand on clique sur le bouton Rechercher
        /* veriflocalisation = 0; */
        window.scrollTo(0, 0); // On remonte la page(pour téléphone)
        if ($("#EdtPrincipal").val() !== "") { // Si il y a une valeur dans le edt
            $('.collapse').collapse('hide'); // On cache la nav 

            // Requete Ajax pour la recherche manuelle
            $.ajax({
                type: "GET",
                url: `https://api-adresse.data.gouv.fr/search/?q= ${$("#EdtPrincipal").val()}`,
                success: onGetReserchSuccess,
                error: onGetReserchError
            });
        } else { // Sinon 
            alert("Adresse absente"); // Pas d'adresse donc msg d'erreur
        }

        // Si succes
        function onGetReserchSuccess(reponse) {
            latitude = reponse.features[0].geometry.coordinates[1]; // On affecte la valeur trouver en latitude
            longitude = reponse.features[0].geometry.coordinates[0]; // On affecte la valeur trouver en longitude
            reponseRecherche = reponse; // On affecte la réponse Ajax
            initContour(); // Puis on rappel initContour
        }

        // Si echéc
        function onGetReserchError() {
            alert("Adresse inconnu"); // On affiche l'erreur
        }
    });

    $("#EdtPrincipal").on("focus", function() { // Si on a le focus sur la zone de texte
        this.value = ""; // On vide le formulaire
    });

    $("#BtnReset").on("click", function() { // Quand on clique sur le bouton reset
        window.scrollTo(0, 0);
        if (navigator.geolocation) {
            // Si on a trouvé une position
            /* veriflocalisation = 1; */
            navigator.geolocation.getCurrentPosition(maPosition, erreurPosition);
        } else {
            // Sinon
            alert("Ce navigateur ne supporte pas la géolocalisation");
        }
    });

    // Création des contours
    function initContour() {
        // Permet de trouver les coordonées des frontières
        // D'un département grâce à un jeu de données                

        // Requete Ajax pour contours-geographiques-des-departements-2019
        $.ajax({
            type: "GET",
            url: `https://public.opendatasoft.com/api/records/1.0/search/?dataset=contours-geographiques-des-departements-2019&q=&facet=insee_dep&facet=nom_reg&facet=nom_dep&geofilter.distance=${latitude}%2C${longitude}`,
            success: onGetPolygoneSuccess,
            error: onGetPolygoneError
        });

        // Si succes
        function onGetPolygoneSuccess(reponse) {
            reponseWScontoursDpt = reponse; // On affecte la réponse Ajax
            if (cpt === 0) { // Si cpt égal à 1
                initMap(); // Puis on appel initMap pour créer la carte
            } else { // Sinon
                moveOverlays(); // On appel changerMap
            }
        }

        // Si echec
        function onGetPolygoneError() {
            alert("Erreur"); // On affiche l'echec
        }
    }

    // Gestion de la map
    function initMap() {
        //Créer un objet "map" et l'insèrer dans l'élément HTML qui a l'ID "macarte" 

        // Affectations des variables
        carte = L.map('macarte', { // La carte
            maxBounds: L.latLngBounds(L.latLng(55.511125799286326, -14.025788027470686), L.latLng(36.42840435364896, 16.968899545991206)), // Le déplacement max
            dragging: true, // Pour rendre la map "touchable" 
            minZoom: 2, // Le zoom min (dézoom)
            maxZoom: 22 // Le zoom max (zoomer)
        }).setView([latitude, longitude], 6); // On met la vue sur la position de l'utilisateur avec un zoom de 6

        cercle = L.circle([latitude, longitude], 100000, { // Pour le cercle de 100km autour du marker
            'color': '#FF7F00', // Couleur du cercle
            'fill': false, // Non remplit
        }).addTo(carte); // On l'ajoute a la carte

        L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(carte); // On l'ajoute a la carte

        // Pour le marqueur
        marqueur(); // On appel marqueur
        creerPolygones(); // On appel creerPolygones
    }

    function moveOverlays() {
        // Change les overlays de l'objet carte

        // Pour les contours
        creerPolygones(); // On rappel creerPolygones
        marqueur(); // On rappel marqueur
        cercle.setLatLng(L.latLng(latitude, longitude)); // On déplace le cercle vers la nouvelle ville
        carte.flyTo([latitude, longitude], 6);
    }

    function marqueur() {
        $.ajax({
            type: "GET",
            url: `https://api-adresse.data.gouv.fr/reverse/?lat=${latitude}&lon=${longitude}`,
            success: onGetCommunesSuccess,
            error: onGetCommunesError
        });


        function onGetCommunesSuccess(reponse) {
            console.log(`https://api-adresse.data.gouv.fr/reverse/?lat=${latitude}&lon=${longitude}`);
            if (cpt === 0) {
                marker = L.marker([latitude, longitude], {
                        icon: L.icon({
                            iconUrl: 'Leaflet/images/marker-icon.png', // Design du marqueur
                            iconSize: [25, 41], // Taille du marqueur
                            iconAnchor: [12, 41], // Point du marqueur
                            popupAnchor: [-3, -76] // Distance entre le marqueur et le popup
                        })
                    }).addTo(carte) // Pour le marqueur
                    .bindPopup(`${reponse.features[0].properties.label} <br> ${reponse.features[0].properties.context}`) // On crée un popup avec des détails de la localisation
                    .openPopup(); // Affichage du popup
                cpt++; // Cpt + 1 pour ne pas rapeler initMap
            } else {
                marker.setLatLng(L.latLng(latitude, longitude)); // On déplace le marqueur vers la nouvelle ville
                // Pour le marqueur
                /* if (veriflocalisation === 1) { */
                marker.setPopupContent(`${reponse.features[0].properties.label} <br> ${reponse.features[0].properties.context}`);
                /* } else { */
                marker.setPopupContent(`${reponse.features[0].properties.label} <br> ${reponse.features[0].properties.context}`); // On change le contenu du Popup
                /* } */
            }
        }

        function onGetCommunesError() {
            alert("Erreur due au service web");
        }
    }

    function creerPolygones() {
        // Permet de créer et d'afficher les polygones

        // Pour remove le / les polygones 
        for (let i = 0; i < arrayLeafletPolygon.length; i++) {
            carte.removeLayer(arrayLeafletPolygon[i]); // on remove chaque polygone 1 par 1
        }
        polygone = []; // On supprime les valeurs à l'intérieur des polygones
        arrayLeafletPolygon = []; // on supprime chaque élément à l'intérieur du tableau

        // Si multiPolygone
        if (reponseWScontoursDpt.records[0].fields.geo_shape.type === "MultiPolygon") {
            for (let i = 0; i < reponseWScontoursDpt.records[0].fields.geo_shape.coordinates.length; i++) {
                polygone.push(reponseWScontoursDpt.records[0].fields.geo_shape.coordinates[i]); // On prend la valeur des coordonnées du départements
            }
            for (let j = 0; j < polygone.length; j++) {
                dataPolygon = []; // On vide les données du polygone d'avant
                for (let i = 0; i < polygone[j][0].length; i++) { // [0] Pour avoir le nombre d'index
                    polygone[j].forEach(function(item) { // On boucle sur chaque tableau lat lng
                        dataPolygon.push(L.latLng(item[i][1], item[i][0])); // On les ajoute au format latLng
                    });
                }
                arrayLeafletPolygon[j] = L.polygon(dataPolygon, { color: 'red' }).addTo(carte); // On ajoute le polygone à la carte et on affecte au tableau des polygones
            }
        } else {
            dataPolygon = []; // On vide les données du polygone d'avant
            polygone = reponseWScontoursDpt.records[0].fields.geo_shape.coordinates[0]; // On prend la valeur des coordonnées du départements
            polygone.forEach(function(item) {
                dataPolygon.push(L.latLng(item[1], item[0])); // On les ajoute au format latLng
            });
            arrayLeafletPolygon[0] = L.polygon(dataPolygon, { color: 'red' }).addTo(carte); // On ajoute le polygone à la carte et on affecte au tableau des polygones
        }
    }
});