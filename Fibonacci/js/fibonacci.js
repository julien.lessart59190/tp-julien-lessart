/*
  Permet d'afficher la suite de fibonacci avec un nombre choisi
  Site Web
  Julien Lessart
  Version 1.0
  29/03/20
*/

function fibonacci() {
    // Cette fonction permet de calculer les x premiers nombres de la suite de Fibonacci
    // Aucun paramètre
    // Une valeur de retour : arrayFibonacci
    let arrayFibonacci = [0, 1], // Tableau pour la suite de fibonacci
        nbsuite = document.getElementById("nbsuite"), // permet de récuperer nbsuite
        tableau = document.getElementById("fibonacci"), // permet de stocker le tableau
        ligne = tableau.insertRow(-1), // Pour inserer les lignes
        tHead = tableau.insertRow(0), // Pour gèrer la haut du tableau
        tabHeader = "", // valeur du header tableau
        colonne = ""; // valeur des colonnes

    // Pour calculer la suite de fibonacci
    for (let i = 1; i <= nbsuite.value - 1; i++) { // Pour i = 1 allant de i à i <= 12 au pas de 1 faire
        arrayFibonacci[i + 1] = arrayFibonacci[i] + arrayFibonacci[i - 1]; // résultat de la suite de fibonacci
    }

    // Pour le tableau
    for (let i = 0; i < arrayFibonacci.length; i++) {
        colonne = ligne.insertCell(i);
        colonne.innerHTML += arrayFibonacci[i];
        tabHeader = tHead.insertCell(i);
        tabHeader.innerHTML += i;
    }
    return arrayFibonacci;
}