/*
  Permet d'afficher l'alphabety
  Site Web
  Julien Lessart
  Version 1.0
  29/03/20
*/

function alpha() {
    // Cette fonction permet d'afficher l'alphabet sur une page web
    // Aucun paramètre
    // Aucune valeur de retour

    let code = 65, // code Unicode qui sera incrémenté de 1
        arrayLettre = [], // Pour l'alphabet
        res = "", // variable de retour et variable transmise à l'html 
        cpt = 0, // Variable pour le tableau
        cssalpha = document.getElementById("alpha"); // Pour récuperer les valeurs de id alpha

    while (code < 91) { // Tant que i = 0 code inférieur à 91 et cpt ++
        arrayLettre[cpt] = String.fromCharCode(code); // Pour l'alphabet
        code++; // code + 1 pour passer à la lettre suivante
        cpt++; // On ajoute 1 au cpt pour changer la valeur de l'index de arrayLettre
    }
    for (let i = 0; i < arrayLettre.length; i++) { // Pour i = 0 allant de i à la taille de arrayLettre au pas de i + 1 faire
        res += arrayLettre[i] + "-"; // Concatenation  du tableau + le caractère "-"
    }
    res = res.substring(0, res.length - 1); // Pour enlevé le "-" à la fin de la chaine de caractère
    document.getElementById("alpha").innerHTML = res; // on récupère la variable res dans id "alpha"

    // Pour le css après l'activation du bouton
    cssalpha.style.textalign = "center";
    cssalpha.style.border = "#5BF740 solid 20px";
    cssalpha.style.width = "410px";
}